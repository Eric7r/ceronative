import { View, Text,StyleSheet } from 'react-native'
import React from 'react'
import { StatusBar } from 'expo-status-bar';

const HolaScreen = () => {
  return (
    <View style={styles.container}>
    <Text>Holi!</Text>
    <StatusBar style="auto" />
  </View>
  )
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
    },
  });

export default HolaScreen